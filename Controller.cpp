#include "Arduino.h"
#include "Motor.h"
#include "Controller.h"

namespace Samonzeweb {
  namespace DfRobotL298OnWheels {

    Controller::Controller(byte powerMinLeft, byte powerMaxLeft,
                                             byte powerMinRight, byte powerMaxRight) {
      LeftMotor = new Motor(directionM1Pin, pwmM1Pin, powerMinLeft, powerMaxLeft);
      RightMotor = new Motor(directionM2Pin, pwmM2Pin, powerMinRight, powerMaxRight);
    }

    // Move forward (straight line)
    void Controller::Forward(byte speed) {
      LeftMotor->Forward(speed);
      RightMotor->Forward(speed);
    }

    // Move backward (straight line)
    void Controller::Backward(byte speed) {
      LeftMotor->Backward(speed);
      RightMotor->Backward(speed);
    }

    // Stop motors
    void Controller::Stop() {
      LeftMotor->Stop();
      RightMotor->Stop();
    }

    // Turn Left (spin)
    void Controller::TurnLeft(byte speed) {
      LeftMotor->Backward(speed);
      RightMotor->Forward(speed);
    }

    // Turn Right (spin)
    void Controller::TurnRight(byte speed) {
      LeftMotor->Forward(speed);
      RightMotor->Backward(speed);
    }

    // Get Left Motor object to custom usage
    Motor *Controller::GetLeftMotor() {
      return LeftMotor;
    }

    // Get Right Motor object to custom usage
    Motor *Controller::GetRightMotor() {
      return RightMotor;
    }

  }
}

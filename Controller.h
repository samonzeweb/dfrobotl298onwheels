#ifndef SAMONZEWEB_DFROBOTL298ONWHEELS_CONTROLLER_H
#define SAMONZEWEB_DFROBOTL298ONWHEELS_CONTROLLER_H

namespace Samonzeweb {
  namespace DfRobotL298OnWheels {

    class Controller {
      public:
        Controller(byte powerMinLeft, byte powerMaxLeft,
                  byte powerMinRight, byte powerMaxRight);
        void Forward(byte speed);
        void Backward(byte speed);
        void Stop();
        void TurnLeft(byte speed);
        void TurnRight(byte speed);
        Motor *GetLeftMotor();
        Motor *GetRightMotor();

      private:
        Motor *LeftMotor, *RightMotor;

        static const byte directionM1Pin = 4;
        static const byte pwmM1Pin       = 5;
        static const byte directionM2Pin = 7;
        static const byte pwmM2Pin       = 6;
    };

  }
}

#endif

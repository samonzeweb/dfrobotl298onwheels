#include "Arduino.h"
#include "Motor.h"

namespace Samonzeweb {
  namespace DfRobotL298OnWheels {

    Motor::Motor(byte initDirectionPin, byte initPwmPin, byte initPowerMin, byte initPowerMax) {
      directionPin = initDirectionPin;
      pwmPin = initPwmPin;
      powerMin = initPowerMin;
      powerMax = initPowerMax;
      pinMode(directionPin, OUTPUT);
      pinMode(pwmPin, OUTPUT);
      Stop();
    }

    void Motor::Forward(byte speed) {
      digitalWrite(directionPin, HIGH);
      analogWrite(pwmPin, Speed2PWM(speed));
    }

    void Motor::Backward(byte speed) {
      digitalWrite(directionPin, LOW);
      analogWrite(pwmPin, Speed2PWM(speed));
    }

    void Motor::Stop() {
      analogWrite(pwmPin, 0);
    }

    // Private methods
    byte Motor::Speed2PWM(byte speed) {
      return map(speed, 0, 255, powerMin, powerMax);
    }

  }
}

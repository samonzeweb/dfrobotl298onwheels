#ifndef SAMONZEWEB_DFROBOTL298ONWHEELS_MOTOR_H
#define SAMONZEWEB_DFROBOTL298ONWHEELS_MOTOR_H

namespace Samonzeweb {
  namespace DfRobotL298OnWheels {

    class Motor {
      public:
        Motor(byte initDirectionPin, byte initPwmPin,
              byte initPowerMin, byte initPowerMax);
        void Forward(byte speed);
        void Backward(byte speed);
        void Stop();

      private:
        byte Speed2PWM(byte speed);

        byte powerMin, powerMax;
        byte directionPin;
        byte pwmPin;
    };

  }
}

#endif

# DFRobot L298 On Whells

This library use a DFRobot shield to control motors on a two wheels robot.

The convention is motor one for left wheel and motor two for right wheel.

There are two kind of objects :
* Controller : the main objeft to control the robot : forward, backward, turn, ...
* Motor : the controller can give access to each motor to do custom control. For simple case just use the controller, not motors.

# Controller constructor and power range

The controller constructor take 4 arguments :
* min and max power for left motor
* min and max power for right motor

These values are Arduino PWM parameters (see analogWrite). For general use determine the minimum values to activate each motors (with too low values motors does not start), and the maximum according to your needs.

If motors does not have the same characteristics you can adjust min and max values separatly to have same real speed.  Use `Formard` method and adjust parameters until the robot goes straight forward.

# Missing

The controller allow the robot to turn in place, without translation. It will be done later.

# Example

```
#include <DfRobotL298OnWheels.h>

using namespace Samonzeweb;

DfRobotL298OnWheels::Controller controller(100, 200, 100, 200);

void setup() {
}

void loop() {
  // Forward slow
  controller.Forward(1);
  delay(2000);
  // Forward fast
  controller.Forward(255);
  delay(2000);
  // Stop
  controller.Stop();
  delay(100);
  // Backward slow
  controller.Backward(1);
  delay(2000);
  // Stop
  controller.Stop();
  delay(100);
  // Turn left slowly in place
  controller.TurnLeft(1);
  delay(5000);
  // Turn right slowly in place
  controller.TurnRight(1);
  delay(5000);
  // Stop
  controller.Stop();
  delay(3000);
}

```
